<?php

namespace Santosh\ShoppingCart\Repositories;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Session;
use Santosh\ShoppingCart\CartInterface\ShoppingCartInterface;


class ShoppingCartRepository implements ShoppingCartInterface
{

    public function __construct(Session $session)
    {
        // $this->session = $session;
    }
    public function add( $model_id, $quantity = null, $price = null, $option= [])
    {
        $cart = [
            $model_id => [
                // "name" => $product->name,
                "quantity" => !is_null($quantity) ? $quantity : 1,
                "price" => $price,
                'options' => !is_null($option) ? $option : null,
                ]
            ];
        session()->put('shopping_cart' , $cart);
        session()->save();
    }
    public function update($id, $quantity =null, $price = null, $option= [])
    {

        $cart = $this->getShoppingCart();
        if(isset($cart[$id])) {
            is_null($quantity) ?  $cart[$id]['quantity']++  :  $cart[$id]['quantity'] = $cart[$id]['quantity']+$quantity;
            session()->put('shopping_cart', $cart);
            session()->save();
        }
        else
        {
            // $this->add($id, $quantity =null, $price = null, $option= []);
            $cart[$id] = [
                // $id => [
                    // "name" => $product->name,
                    "quantity" => !is_null($quantity) ? $quantity : 1,
                    "price" => $price,
                    'options' => !is_null($option) ? $option : null,
                    // ]
                ];
            session()->put('shopping_cart' , $cart);
            session()->save();
        }




    }
    public function remove($id)
    {
        $cart = $this->getShoppingCart();
        if(isset($cart[$id])) {
            unset($cart[$id]);
            session()->put('shopping_cart', $cart);
            session()->save();
            return true;
        }
        return false;

    }
    public function removeAllData()
    {
         session()->forget('shopping_cart');

         session()->save();
         Artisan::call('cache:clear');


    }
    public function content()
    {
        return $this->getShoppingCart();

    }

    public function checkCartExist($product_id)
    {
        if(session()->has('shopping_cart'))
        {
            // dd('caert');
            return true;
        }
    }

    public function getShoppingCart()
    {
        return session()->get('shopping_cart');
    }
    public function setShoppingCart()
    {
        // dd('fdfd');
       return  session()->put('shopping_cart' , []);
    }

    public function checkItemExist($product_id)
    {
       $cart = $this->getShoppingCart();
        if(isset($cart[$product_id])) {
            // dd(true);
            return true;
        }
        // dd(false);
        return false;
    }

    public function count()
    {
        if(!is_null($this->getShoppingCart())) :
            return count($this->getShoppingCart());
        endif ;
            return 0;

    }

    public function updateCartOptions($key,$option = [])
    {
        $cart = $this->getShoppingCart();
        if(isset($cart[$key])) {
            // is_null($quantity) ?  $cart[$id]['quantity']++  :  $cart[$id]['quantity'] = $cart[$id]['quantity']+$quantity;
            $cart[$key]['options'] = $option;
            session()->put('shopping_cart', $cart);
            session()->save();
        }   
    }

}
