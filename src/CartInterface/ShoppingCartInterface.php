<?php

namespace Santosh\ShoppingCart\CartInterface;

interface ShoppingCartInterface
{

    public function add($model_id, $quantity = null, $price = null, $option= []);
    public function update($id, $quantity =null);
    public function remove($id);
    public function content();
    public function count();

}

