<?php

namespace Santosh\ShoppingCart;

use Santosh\ShoppingCart\CartInterface\ShoppingCartInterface;

class ShoppingCart
{
    public function __construct(ShoppingCartInterface $shoppingCartInterface)
    {
        $this->shoppingCartInterface = $shoppingCartInterface;
    }
    public  function addItem ($model_id, $quantity=null, $price, $option= [])
    {
        $isCart = $this->shoppingCartInterface->checkCartExist($model_id);
        if($isCart)
        {
            $itemExist = $this->shoppingCartInterface->checkItemExist($model_id);
            // dd($itemExist);
            // $itemExist ? $this->shoppingCartInterface->update($model_id, $quantity =null, $price = null, $option= []) :   $this->shoppingCartInterface->add( $model_id, $quantity, $price, $option);
            $this->shoppingCartInterface->update($model_id, $quantity, $price , $option= []);
            return 'cart quantity updated in exsting cart successfully';
        }
        elseif(!$isCart)
        {
            $this->shoppingCartInterface->setShoppingCart();
            $this->shoppingCartInterface->add( $model_id, $quantity, $price, $option);
            return 'new data in session added successfully';
        }

    }

    public function getContent()
    {
        return  $this->shoppingCartInterface->content();
    }
    public function removeCart()
    {
        $this->shoppingCartInterface->removeAllData();
        return 'sucessfully remove a cart data';
    }
    public function remove($id)
    {
        $remove = $this->shoppingCartInterface->remove($id);
        return $remove ? 'sucessfully remove a cart data' : 'trouble in remove item from cart';
    }
    public function count()
    {
       return $this->shoppingCartInterface->count();
    }

    public function subTotal()
    {
        // dd($this->getContent());
        $amount = (float) 0;
        foreach($this->getContent() as $cart)
        {
            $amount = $amount + (float) $cart['price']*$cart['quantity'];
        }
        // dd($amount);
        return $amount ;
    }
}
