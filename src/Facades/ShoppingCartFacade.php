<?php

namespace Santosh\ShoppingCart\Facades;

use Illuminate\Support\Facades\Facade;

class ShoppingCartFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'shoppingcart';
    }
}
