<?php

namespace Santosh\ShoppingCart ;

use Illuminate\Support\ServiceProvider;
use Santosh\ShoppingCart\CartInterface\ShoppingCartInterface;
use Santosh\ShoppingCart\Repositories\ShoppingCartRepository;

class ShoppingCartServiceProvider extends ServiceProvider
{

    public function boot()
    {

    }

    public function register()
    {

        $this->app->bind('shoppingcart',ShoppingCart::class);
        $this->app->bind(ShoppingCartInterface::class, ShoppingCartRepository::class);

    }
}
